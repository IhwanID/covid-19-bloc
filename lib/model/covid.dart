import 'package:json_annotation/json_annotation.dart';

part 'covid.g.dart';

@JsonSerializable()
class CovidResponse {
  CovidResponse(this.confirmed, this.recovered, this.deaths);
  factory CovidResponse.fromJson(Map<String, dynamic> json) =>
      _$CovidResponseFromJson(json);
  Map<String, dynamic> toJson() => _$CovidResponseToJson(this);

  final Value confirmed;
  final Value recovered;
  final Value deaths;
}

@JsonSerializable()
class Value {
  Value(this.value);

  factory Value.fromJson(Map<String, dynamic> json) => _$ValueFromJson(json);
  Map<String, dynamic> toJson() => _$ValueToJson(this);

  final int value;
}
