import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:covid19/model/covid.dart';
import 'package:covid19/service/covid_repository.dart';
import './bloc.dart';

class CovidBloc extends Bloc<CovidEvent, CovidState> {
  final CovidRepository repository;
  CovidBloc({this.repository});

  @override
  CovidState get initialState => InitialCovidState();

  @override
  Stream<CovidState> mapEventToState(
    CovidEvent event,
  ) async* {
    if (event is FetchCovidData) {
      yield LoadingCovidState();
      try {
        final CovidResponse covid = await repository.getData();
        yield LoadedCovidState(covid: covid);
      } catch (_) {
        yield ErrorCovidState();
      }
    }
  }
}
