import 'package:covid19/model/covid.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class CovidState extends Equatable {
  const CovidState();
  @override
  List<Object> get props => [];
}

class InitialCovidState extends CovidState {}

class LoadingCovidState extends CovidState {}

class LoadedCovidState extends CovidState {
  final CovidResponse covid;

  const LoadedCovidState({@required this.covid}) : assert(covid != null);

  @override
  List<Object> get props => [covid];
}

class ErrorCovidState extends CovidState {}
