import 'dart:convert';

import 'package:covid19/model/covid.dart';
import 'package:dio/dio.dart';

class CovidRepository {
  Dio get dio => _dio();
  Dio _dio() {
    final options = BaseOptions(
      baseUrl: "https://covid19.mathdro.id/api/countries/",
      connectTimeout: 5000,
      receiveTimeout: 3000,
      contentType: "application/json;charset=utf-8",
    );

    var dio = Dio(options);
    return dio;
  }

  Future<CovidResponse> getData() async {
    final response = await dio.get('id');

    if (response.statusCode != 200) {
      throw Exception('error getting covid data');
    }

    return CovidResponse.fromJson(response.data);
  }
}
