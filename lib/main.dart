import 'package:covid19/bloc/bloc.dart';
import 'package:covid19/service/covid_repository.dart';
import 'package:covid19/ui/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  final CovidRepository repository = CovidRepository();
  runApp(BlocProvider<CovidBloc>(
      create: (context) => CovidBloc(repository: repository), child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomeScreen(),
    );
  }
}
