// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'covid.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CovidResponse _$CovidResponseFromJson(Map<String, dynamic> json) {
  return CovidResponse(
    json['confirmed'] == null
        ? null
        : Value.fromJson(json['confirmed'] as Map<String, dynamic>),
    json['recovered'] == null
        ? null
        : Value.fromJson(json['recovered'] as Map<String, dynamic>),
    json['deaths'] == null
        ? null
        : Value.fromJson(json['deaths'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CovidResponseToJson(CovidResponse instance) =>
    <String, dynamic>{
      'confirmed': instance.confirmed,
      'recovered': instance.recovered,
      'deaths': instance.deaths,
    };

Value _$ValueFromJson(Map<String, dynamic> json) {
  return Value(
    json['value'] as int,
  );
}

Map<String, dynamic> _$ValueToJson(Value instance) => <String, dynamic>{
      'value': instance.value,
    };
