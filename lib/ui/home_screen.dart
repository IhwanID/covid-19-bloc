import 'package:covid19/bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<CovidBloc>(context).add(FetchCovidData());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<CovidBloc, CovidState>(builder: (context, state) {
        if (state is LoadingCovidState) {
          return Center(child: CircularProgressIndicator());
        }

        if (state is InitialCovidState) {
          return Center(child: CircularProgressIndicator());
        }

        if (state is LoadedCovidState) {
          var data = state.covid;
          return Center(child: Text("""
          confirmed: ${data.confirmed.value}
          recovered: ${data.recovered.value}
          deaths: ${data.deaths.value}"""));
        }
        return Center(child: CircularProgressIndicator());
      }),
    );
  }
}
